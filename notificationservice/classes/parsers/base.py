import datetime
from dbcommons.classes.models.events.activity import ActivityLog
import json

class Parser(object): 

	def __init__(self):
		self.data = []

	def process(self, filePath = None):		
		self.fileToDict(filePath)
		self.saveInDb()

	def fileToDict(self, filePath):
		pass
		
	def saveInDb(self):
		pass	

	def get_activity(self, params):
		activity = ActivityLog()
		if 'actor' in params:
			activity.actor = params['actor']
		if 'verb' in params:
			activity.verb = params['verb']
		if 'object' in params:
			activity.object = params['object']
		if 'target' in params:
			activity.target = params['target']
		if 'metadata' in params:
			activity.meta = json.dumps(params['metadata'])
		if 'occurred' in params:
			activity.occurred = datetime.datetime.fromtimestamp(params['occurred'])
		if 'version' in params:
			activity.version = params['version']
		else:
			activity.version = 1
		return activity