import csv, traceback
from base import Parser
from dbcommons.models import DBSession

class BookItem(Parser):

	def fileToDict(self, filePath):		
		with open(filePath, 'r') as f:
			reader = csv.reader(f, delimiter=',')
			itemIds = []
			userItems = {}			
			for index, row in enumerate(reader):
				try:		
					if index == 0:
						itemIds = [int(elem) for elem in row if elem != 'user_id']					
					else:
						row[0] = int(row[0])
						userItems[row[0]] = []					
						for colIndex in range(1,len(row)):
							if row[colIndex] != '0':
								try:							
									userItems[row[0]].append(itemIds[colIndex-1])
								except:
									traceback.print_exc()
				except:
					traceback.print_exc()					
					return
		self.data = userItems				

	def saveInDb(self):	
		session = DBSession()	
		for user_id, item_ids in self.data.iteritems():			
			for item_id in item_ids:
				newActivityObj = self.get_activity({'verb': 'bookitem', 'actor':'user:%d' %(user_id), 'object': 'item:%d' %(item_id)})
				try:
					session.add(newActivityObj)
					session.commit()
				except:
					traceback.print_exc()
					session.rollback()
