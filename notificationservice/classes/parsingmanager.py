import time
from commons.classes.utilities import convertStringToClassFunction, getClass
import os
from dbcommons.models import DBSession, initialize_sql
from sqlalchemy.engine import create_engine

parsers = [
			#{'name': 'notificationservice.classes.parsers.bookitem.BookItem', 'fileNameFmt': 'Booking button on item clicked.csv'}
		]

	
def getAllValidFiles():
	#todaysDate = time.strftime("%Y%m%d")
	todaysDate = '20141202'
	allFiles = os.listdir('/Users/mg123/Downloads/localytics')
	return ['/Users/mg123/Downloads/localytics/'+f for f in allFiles if f[:8] == todaysDate]


def process():
	fileObj = None	
	allValidFiles = getAllValidFiles()	
	fPath = None
	for parser in parsers:		
		classObj = getClass(parser['name'])()
		if 'fileNameFmt' in parser:			
			for path in allValidFiles:
				if parser['fileNameFmt'] in path:
					fPath = path
					break
		classObj.process(filePath = fPath)
		


if __name__ == "__main__":
	connect_string = "mysql://root@localhost/mygola?charset=utf8&use_unicode=0"
	pool_size = 10
	engine = create_engine(connect_string, pool_size = pool_size, max_overflow=5, pool_recycle=3600, echo_pool=False, echo=False)
	initialize_sql(engine)
	process()