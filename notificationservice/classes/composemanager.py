from dbcommons.classes.models.events.activity import ActivityLog
from dbcommons.classes.models.events.notifications import Notifications
from dbcommons.classes.models.pd.iospushnotification import IOSActiveUsers
import time, requests, datetime
from commons.classes.utilities import convertStringToClassFunction, getClass
import os, traceback, json
from dbcommons.models import DBSession, initialize_sql
from sqlalchemy.engine import create_engine

PROD_URL = "http://perfectday.mygola.com/services/sendnotification?user_ids=%d&msg=%s&extra=%s"
TEST_URL = "http://10.0.0.123:8080/services/sendnotification?user_ids=%d&msg=%s&extra=%s"

constraints = []
composersMap = {
				'additem': [								
								{'name':'notificationservice.classes.composers.additemnotbooked.AddItemNotBooked'},
								{'name':'notificationservice.classes.composers.additem.AddItem'}
							],
				'bookitem': [
								#{'name':'notificationservice.classes.composers.bookitem.BookItem'}
							]
			}
	

def getAllUserEvents():
	session = DBSession()
	todaysDate = time.strftime("%Y%m%d")
	yesterdaysDate = (datetime.date.today() - datetime.timedelta(1)).strftime('%Y%m%d')		
	prevMonthsDate = (datetime.date.today() - datetime.timedelta(30)).strftime('%Y%m%d')		
	users = {}
	validActivities = session.query(ActivityLog).filter(ActivityLog.verb.in_(['additem'])).filter(ActivityLog.created < todaysDate).filter(ActivityLog.created > prevMonthsDate).all()			
	
	#validActivities = session.query(ActivityLog).filter(ActivityLog.verb.in_(['additem'])).filter(ActivityLog.created >= yesterdaysDate).filter(ActivityLog.created < todaysDate).all()	

	for activity in validActivities:
		if activity.actor[5:] not in users:
			users[activity.actor[5:]] = []
		users[activity.actor[5:]].append(activity)	
	return users
	
#should check for conditions/rules
def addNotificationsToQueue(user_id, notifications):
	session = DBSession()
	isIosUser = session.query(IOSActiveUsers).filter(IOSActiveUsers.user_id == user_id).first()
	if isIosUser:		
		for notification in notifications:		
			notification['user_id'] = user_id
			notificationObj = Notifications()
			notificationObj.state = 0
			notificationObj.type = 'ios_push_notification'
			notificationObj.data = json.dumps(notification)
			print notificationObj
			try:				
				print session.add(notificationObj)
				print session.commit()
			except:
				traceback.print_exc()
				session.rollback()
			# notificationObj.status = 0
			# notificationObj.status = 0
			# url = TEST_URL %(int(user_id), notification['msg'], json.dumps(notification['extra']))
			# print url
			#print requests.get(url)

def filterNotifications(notifications):	
	if len(notifications) > 1:
		return notifications[0:1]
	return notifications

#filter notification
#add to queue/db instead of directly firing
def process():
	userEventsMap = getAllUserEvents()	
	for user_id, userEvents in userEventsMap.iteritems():
		notifications = []
		for userEvent in userEvents:
			composers = composersMap[userEvent.verb]
			for composer in composers:
				try:		
					classObj = getClass(composer['name'])()					
					newNotification = classObj.process(userEvent)					
					if newNotification:	
						notifications.append(newNotification)
				except:					
					traceback.print_exc()
		notifications = filterNotifications(notifications)							
		addNotificationsToQueue(user_id, notifications)		

if __name__ == "__main__":
	connect_string = "mysql://root@localhost/mygola?charset=utf8&use_unicode=0"
	pool_size = 10
	engine = create_engine(connect_string, pool_size = pool_size, max_overflow=5, pool_recycle=3600, echo_pool=False, echo=False)
	initialize_sql(engine)
	process()