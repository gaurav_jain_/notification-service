import datetime
import json
import ConfigParser
import os

class Composer(object): 

	def __init__(self):
		pass

	def process(self, userEvent):
		print 'inside process of base'
		self.userEvent = userEvent
		return self.getNotificationParams()

	def getNotificationParams(self):
		pass	

	def getTemplateInfo(self):
		templateName = self.__class__.__name__.lower() + '.ini'
		config = ConfigParser.SafeConfigParser()
		config.read(os.path.realpath('notificationservice/configs/'+ templateName))	
		msg = config.get('info', 'msg')
		extra = config.get('info', 'extra')		
		return (msg, extra)
		