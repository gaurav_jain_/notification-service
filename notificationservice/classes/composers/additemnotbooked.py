#send u have added item but u have not booked it

import datetime
import json
import requests
from dbcommons.classes.models.user.user import User
from dbcommons.classes.models.item.item import Item
from dbcommons.classes.models.item.metadata import ItemMetadata
from base import Composer
from dbcommons.models import DBSession
from dbcommons.classes.models.events.activity import ActivityLog

class AddItemNotBooked(Composer): 

	def __init__(self):
		self.data = []

	def getNotificationParams(self):
		session = DBSession()	
		userId = self.userEvent.actor[5:]
		itemId = self.userEvent.object[5:]
		tomorrowsDate = (datetime.date.today() - datetime.timedelta(-1)).strftime('%Y%m%d')
		yesterdaysDate = (datetime.date.today() - datetime.timedelta(1)).strftime('%Y%m%d')		
		try:		
			itemDate = json.loads(self.userEvent.meta)['planDate']
		except:			
			itemDate = tomorrowsDate			
		itemDate = itemDate.replace('-','')		
		itemDateWithHyphen = "'" + str(itemDate[0:4] + '-' + itemDate[4:6] + '-' + itemDate[6:8]) + "'" 		
		#todaysDate = time.strftime("%Y%m%d")		
		bookingCompletedObj = session.query(ActivityLog).filter(ActivityLog.verb == 'bookingcompletedbyuser').filter(ActivityLog.created >= self.userEvent.created).filter(ActivityLog.actor == self.userEvent.actor).filter(ActivityLog.object == self.userEvent.object).first()

		itemWithBookingTypeInfo = session.query(Item, ItemMetadata).join(Item.itemMetadata).filter(Item.id==itemId).filter(ItemMetadata.key == 'booking_type').filter(ItemMetadata.value.in_([1,2])).first()						
		#if not bookingCompletedObj and itemDate == todaysDate:						
		itemAddedOn = self.userEvent.created.strftime('%Y%m%d')		
		if not bookingCompletedObj and itemWithBookingTypeInfo and itemDate >= tomorrowsDate and itemAddedOn >= yesterdaysDate:								
			item = itemWithBookingTypeInfo[0]						
			user = session.query(User).filter(User.id==userId).first()
			msg, extra = self.getTemplateInfo()
			if user.name:
				msg = msg.replace('<user_name>',user.name)
			if item.title:	
				msg = msg.replace('<item_name>', item.title)
			extra = extra.replace('<item_id>', itemId).replace('<date>', itemDateWithHyphen)
			extra = eval(extra)
			return {'msg': msg, 'extra': extra}		
		