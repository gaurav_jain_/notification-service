#send u added item for today

import datetime,time
import json
import requests
from dbcommons.classes.models.user.user import User
from dbcommons.classes.models.item.item import Item
from base import Composer
from dbcommons.models import DBSession

class AddItem(Composer): 

	def __init__(self):
		self.data = []

	def getNotificationParams(self):
		#if todays = plan's date then only send notification
		session = DBSession()	
		userId = self.userEvent.actor[5:]
		itemId = self.userEvent.object[5:]		
		tomorrowsDate = (datetime.date.today() - datetime.timedelta(-1)).strftime('%Y%m%d')
		try:		
			itemDate = json.loads(self.userEvent.meta)['planDate']			
			itemDate = itemDate.replace('-','')		
			itemDateWithHyphen = "'" + str(itemDate[0:4] + '-' + itemDate[4:6] + '-' + itemDate[6:8]) + "'" 		
			itemAddedOn = self.userEvent.created.strftime('%Y%m%d')
			if itemDate == tomorrowsDate:
				user = session.query(User).filter(User.id==userId).first()
				item = session.query(Item).filter(Item.id==itemId).first()
				msg, extra = self.getTemplateInfo()		
				if user.name:
					msg = msg.replace('<user_name>',user.name)
				if item.title:	
					msg = msg.replace('<item_name>', item.title)
				extra = extra.replace('<item_id>', itemId).replace('<date>', itemDateWithHyphen)
				extra = eval(extra)
				return {'msg': msg, 'extra': extra}			
		except:
			pass			
				