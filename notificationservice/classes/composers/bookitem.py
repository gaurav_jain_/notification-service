#send u have clicked booked item but u have not booked it
import datetime, time
import json
import requests
from dbcommons.classes.models.user.user import User
from dbcommons.classes.models.item.item import Item
from base import Composer
from dbcommons.models import DBSession
from dbcommons.classes.models.events.activity import ActivityLog

class BookItem(Composer): 

	def __init__(self):
		self.data = []

	def getNotificationParams(self):
		#if item is not booked till the plan's date send notification
		#add server side booking complete event in activity_logs
		session = DBSession()	
		userId = self.userEvent.actor[5:]
		itemId = self.userEvent.object[5:]
		#itemDate = json.loads(self.userEvent.meta)['planDate']
		#todaysDate = time.strftime("%Y%m%d")
		bookingCompletedObj = session.query(ActivityLog).filter(ActivityLog.verb == 'bookingcompletedbyuser').filter(ActivityLog.created >= self.userEvent.created).filter(ActivityLog.actor == self.userEvent.actor).filter(ActivityLog.object == self.userEvent.object).first()
		#if not bookingCompletedObj and itemDate == todaysDate:		
		if not bookingCompletedObj:		
			item = session.query(Item).filter(Item.id==itemId).first()
			user = session.query(User).filter(User.id==userId).first()			
			msg, extra = self.getTemplateInfo()
			if user.name:
				msg = msg.replace('<user_name>',user.name)
			if item.title:	
				msg = msg.replace('<item_name>', item.title)
			extra = extra.replace('<item_id>', itemId)
			extra = eval(extra)		
			return {'msg': msg, 'extra': extra}