from dbcommons.classes.models.events.notifications import Notifications
from dbcommons.models import DBSession, initialize_sql
from sqlalchemy.engine import create_engine
import time, requests, json, traceback, urllib

PROD_URL = "http://perfectday.mygola.com/services/sendnotification?user_ids=%d&msg=%s&title=&extra=%s"
TEST_URL = "http://10.0.0.123:8080/services/sendnotification?user_ids=%d&msg=%s&title=&extra=%s"

def process():
	session = DBSession()
	#prevDaysTime = time.strftime("%Y-%m-%d %H:%M:%S", localtime(time.time()-24*60*60))
	#prevDaysTime = '2014-12-07 14:36:26'
	#pendingNotifications = session.query(Notifications).filter(Notifications.created >= prevDaysTime).filter(Notifications.state == 0).all()
	pendingNotifications = session.query(Notifications).filter(Notifications.state == 0).all()
	for notification in pendingNotifications:
		notificationData = json.loads(notification.data)
		url = TEST_URL %(int(notificationData['user_id']), urllib.quote(notificationData['msg'].encode('utf-8')), json.dumps(notificationData['extra']))
		#print url
		print requests.get(url)
		notification.state = 1
		try:
			session.commit()
		except:
			traceback.print_exc()
			session.rollback()



if __name__ == "__main__":
	connect_string = "mysql://root@localhost/mygola?charset=utf8&use_unicode=0"
	pool_size = 10
	engine = create_engine(connect_string, pool_size = pool_size, max_overflow=5, pool_recycle=3600, echo_pool=False, echo=False)
	initialize_sql(engine)
	process()