from dbcommons.models import initialize_sql
from pyramid.config import Configurator
from pyramid.renderers import JSON
from sqlalchemy.engine import create_engine

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    connect_string = settings['sqlalchemy.url']
    try:
        pool_size = int(settings['pool.size'])
    except:
        pool_size=10
    engine = create_engine(connect_string, pool_size = pool_size, max_overflow=5, pool_recycle=3600, echo_pool=False, echo=False)
    initialize_sql(engine)
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.scan()
    return config.make_wsgi_app()
